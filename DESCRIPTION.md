OpenProject is a sophisticated project management application. Get your projects done with a multitude of features to support your teams.

## Features

### Work packages

* Organize your projects’ requirements, tasks, bugs, risks and much more
* Assign responsibilities, keep track of due dates, see the change history
* Configure your individual workflows

### Timelines

* Easily track your project phases and milestones
* Have customized timelines tailored to your needs
* Easily track changes and dependencies

### Agile and Scrum

* Manage your Agile teams with digital backlogs
* Support your Daily meetings with an online task board
* See the sprint progress in a burndown chart

### Wiki

* Document and share project information in a wiki
* Embed attachments, such as images, and timeline reports
* See the change history

### Time and costs

* Create and monitor your project budgets
* Log time or units spent to work packages
* Generate aggregated cost reports

### More project collaboration software features

* Share documents with integrated version control
* Manage meetings with agendas and minutes
* Communicate news within your projects
* Discuss topics in project boards
