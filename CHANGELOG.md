[0.1.0]
* Initial version

[0.1.1]
* Update to upstream version 6.0.4

[0.1.2]
* Update to upstream version 6.0.5

[0.2.0]
* Update to upstream version 6.1.0

[0.2.1]
* Update to upstream version 6.1.1

[0.2.2]
* Update to upstream version 6.1.2

[0.2.3]
* Update to upstream version 6.1.4

[0.3.0]
* Update to base image 0.10.0

[0.3.1]
* Update to upstream version 6.1.5

[0.3.2]
* Fix SMTP settings
* Fix app crash by not relying on tmp resources

[1.0.0]
* Update to upstream version 7.3.2

[1.0.1]
* Fix remote repository support
* Ensure hostname is picked up on first startup
* Correctly set the email notification from address

[1.1.0]
* Update to OpenProject 7.4

[1.1.1]
* Update to OpenProject 7.4.1

[1.1.2]
* Update to OpenProject 7.4.2

[1.1.3]
* Update to OpenProject 7.4.4

[1.1.4]
* Fix file upload issue

[1.1.5]
* Update to OpenProject 7.4.5

[1.1.6]
* Update OpenProject to 7.4.6

[1.1.7]
* Update OpenProject to 7.4.7

[2.0.0]
* Update OpenProject to 8.2.0

[2.0.1]
* Update OpenProject to 8.2.1
* Fixed: Long heading in forum overlaps buttons [#28839]
* Fixed: Wrong error message when updating teaser element on Project Overview page [#29011]
* Fixed: Page not found displays old OpenProject favicon icon [#29026]
* Fixed: Incorrect spacings on WP full view (mobile) [#29154]
* Fixed: Mobile notifications hidden by topbar [#29171]
* Fixed: Missing colors for status selector and type indicator in Chrome pdf export [#29175]
* Fixed: Unnecessary truncation on work package page (split screen) [#29191]
* Fixed: Cost reports: broken scrolling behaviour [#29204]
* Fixed: Title of default views changes after visiting a work package full view [#29211]
* Fixed: Two column layout not applied on page reload [#29238]
* Fixed: Work package table header not sticky on MS Edge, Opera, Safari [#29239]
* Fixed: Work package table hierarchy arrows incorrectly indented on MS Edge [#29242]
* Changed: When creating version on “Backlogs” page redirect to Backlogs page (not project settings) [#29182]

[2.1.0]
* Update OpenProject to 8.3.0

[2.1.1]
* Update OpenProject to 8.3.1

[2.1.2]
* Update OpenProject to 8.3.2

[2.2.0]
* Update OpenProject to 9.0.2

[2.2.1]
* Update OpenProject to 9.0.3

[3.0.0]
* Update OpenProject to 10.0.2
* OpenProject now uses PostgreSQL instead of MySQL
* IMPORTANT: Database migration should happen automatically if not please contact support

[3.1.0]
* Update OpenProject to 10.2.1

[3.1.1]
* Update OpenProject to 10.2.2
* Fixed: Outline of display-field extends table cell width [#31214]
* Fixed: SQL error in work package view [#31667]
* Fixed: Error in OpenProject::Patches::ActiveRecordJoinPartPatch module after ActiveRecord updating to 6 version [#31673]
* Fixed: [WorkPackages] “group by”-header line to short [#31720]
* Fixed: Second level menu of boards is not reachable on first click [#31721]
* Fixed: Doubled scrollbar in sidebar when “Buy Now” teaser is activated [#31729]
* Fixed: Main menu width is lost after closing it [#31730]
* Fixed: Dropdown for table highlighting configuration is off place [#31732]
* Fixed: Double click on WP row not working [#31737]
* Fixed: Unverified CSRF request in create budget form [#31739]
* Fixed: Selected long cf values are reduced to … in ng-select [#31765]
* Fixed: Webhook is failing/crashing due to NameError [#31809]
* Fixed: API v3 /relations/:id does not check permissions [#31853 ]
* Fixed: Tabnabbing on wiki pages [#31817 ]

[3.2.0]
* Update OpenProject to 10.3.0
* Changed: [Watchers] Removing watcher sends email to removed watcher [#21304]
* Changed: Work packages tiles view for small screens [#31631]
* Changed: Scroll header out of view on mobile [#31699]
* Changed: Limit width of sidebar on mobile [#31701]
* Changed: Remove borders of card view on mobile work package page [#31702]
* Changed: Merge toolbar title and buttons into one line on mobile [#31736]
* Fixed: Translation error [#29828]
* Fixed: Attachment API tries to call to_json on binary data if attachment file has mime-type application/json [#31661]
* Fixed: Using "Back" button on WP show page reloads wp table [#31698]
* Fixed: Project list input has a zoom effect on iOS [#31700]
* Fixed: Board list buttons overlap the sidebar on mobile [#31722]
* Fixed: Bottom bar overlaps content on mobile [#31723]
* Fixed: Lists are out of place in rtl-CF [#31731]
* Fixed: More than two tildes (~) in markdown break CKEditor [#31749]
* Fixed: Avatar for user option "none" shown [#31764]
* Fixed: Closing an error message during registration closes whole form [#31808]
* Fixed: Version wiki page setting allows input (URL) breaking the version view [#31845]
* Fixed: Derived Values for Estimated Cut off [#31852]
* Fixed: Column height for work packages changes based on content [#31874]
* Fixed: Error 500 when non-admin tries to sort by project "Status" column [#31889]
* Fixed: On mobile side menu is cut off when opening side menu [#31891]

[3.2.1]
* Update OpenProject to 10.3.1
* Fixed: Activity numbers only partly shown on mobile [#31892]
* Fixed: Text in custom field not correctly displayed [#31896]
* Fixed: New Wiki pages are not shown [#31907]
* Fixed: Date separator (dash) misplaced [#31926]

[3.3.0]
* Update OpenProject to 10.4.0

[3.3.1]
* Update OpenProject to 10.4.1
* Fix HTTPS warning in admin panel

[3.4.0]
* Update OpenProject to 10.5.1
* Update to new Cloudron base image v2

[3.5.0]
* Update OpenProject to 10.6.0

[3.5.1]
* Update OpenProject to 10.6.1

[3.6.0]
* Update OpenProject to 10.6.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v10.6.2)

[3.6.1]
* Update OpenProject to 10.6.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v10.6.3)

[3.6.2]
* Update OpenProject to 10.6.4
* [Full changelog](https://github.com/opf/openproject/releases/tag/v10.6.4)

[3.6.3]
* Update OpenProject to 10.6.5
* Fixed: Comments get repeated when entering several comments within 5 minutes [#33744]
* Fixed: Wrong (abusive) translations on avatar change page in russian localization. [#33888]
* [Full changelog](https://github.com/opf/openproject/releases/tag/v10.6.5)

[3.7.0]
* Update OpenProject to 11.0.0

[3.7.1]
* Update OpenProject to 11.0.1
* Fixed: Gantt chart: styles conflict between last active work package and hovered work package [#34126]
* Fixed: Displaced datepicker for custom fields in the project dashboard [#34253]
* Fixed: Date field for version cannot be set / edited from Backlogs page anymore [#34436]
* Fixed: Gantt chart and table scroll independently (e.g. when creating subtask) [#34828]
* Fixed: Error message when copying boards not scrollable [#34842]
* Fixed: Burndown button shown on task board that just reloads page [#34880]
* Fixed: Datepicker doesn't work on Firefox [#34910]
* Fixed: Highlighting in date picker incorrect [#34929]
* Fixed: Migration to 11.0.0 fails for users having had MySQL [#34933]
* Fixed: Settingpage "API": Page not found [#34938]
* Fixed: User language is not updating through api [#34964]
* Fixed: Filter on CF string brakes form in split screen. [#34987]
* Fixed: API Settings Page Broken - ActionController::RoutingError (uninitialized constant Settings::ApiController [#34994]
* Fixed: Repo Management Not Working [#35011]

[3.7.2]
* Update OpenProject to 11.0.2
* Fixed: Typos / missing words in German translation when deleting user [#35072]
* Fixed: Default docker-compose.yml is has tag 10 [#35093]
* Fixed: Copied wiki attachments have original author while it should be the copying user [#35126]
* Fixed: Slashes in wiki page titles break “Activity” view [#35132]

[3.7.3]
* Update OpenProject to 11.0.3

[3.7.4]
* Update OpenProject to 11.0.4

[3.8.0]
* Update OpenProject to 11.1.0
* Improved styling of the WYSIWYG text editor
* Direct display of user name for @notifications
* Display project milestones aggregated for projects in a single row in Gantt chart
* Collapse all button for groups in the work packages list
* [Full changelog](https://github.com/opf/openproject/releases/tag/v11.1.0)

[3.8.1]
* Update OpenProject to 11.1.1

[3.8.2]
* Update OpenProject to 11.1.2
* Run apache and passenger always as www-data

[3.8.3]
* Update OpenProject to 11.1.2
* Run apache and passenger always as www-data

[3.8.4]
* Run cron task as www-data

[3.8.5]
* Update OpenProject to 11.1.3
* Update base image to version 3
* Filter gets removed (ERIK@Staging) [#34003]
* S3 presigned URL cached for 7 days does not work with IAM roles and is a security issue [#35739]
* Images directly uploaded to s3 are not displayed within new tab [#36018]
* Selecting "Atom" in export menu throws cryptic error [#36052]
* Creating new synchronized groups from filters raises error if group name too long [#36081]

[3.8.6]
* Update OpenProject to 11.1.4
* Fixed: Some settings can be written without authorization [#36254]
* [Changelog](https://docs.openproject.org/release-notes/11-1-4/)

[3.9.0]
* Update OpenProject to 11.2.0
* https://docs.openproject.org/release-notes/11-2-0/
* Extended filter options by parent and ID
* Distribution of user administration
* UI and UX improvements

[3.9.1]
* Update OpenProject to 11.2.1
* Fixed: Inbound Emails - Email headers not handled correctly [#35834]
* Fixed: Configuration and display of days in My spent time widget on my Page do not match [#35920]
* Fixed: All data lost when switching from details view to fullscreen view while creating a work package [#35968]
* Fixed: missing translation "ja.js.units.hour.one" [#36269]
* Fixed: Internal Error when Wiki entry contains single "!" [#36345]
* Fixed: Missing word in deletion confirmation for placeholder users [#36516]
* Fixed: Cannot select assignee on WP create when filtering for multi-select custom field of type list [#36607]
* Fixed: Error message in wrong language [#36688]

[3.9.2]
* Update OpenProject to 11.2.3
* Fixed: Task "Start Date in less than" filter does not include dates in the past. [#34513]
* Fixed: MimeMagic deprecated [#36725]
* Fixed: date entry of custom date field cannot be deleted [#36727]
* Fixed: Assign random password broken if "Number of most recently used passwords banned for reuse" enabled [#36746]
* Fixed: Work package table and Gantt not aligned [#36764]
* Fixed: BIM group seed data is always applied [#36796]

[3.9.3]
* Update OpenProject to 11.2.4
* Fixed: Pagination in search results [#35045]
* Fixed: Images in Dashboard are stretched in Safari [#36547]
* Fixed: Docker[ proxy apache2]: Report 502 if the file is uploaded [#36685]
* Fixed: Refresh problem after archiving projects [#36978]
* Fixed: Delayed request with system user fails due to temporary admin permissions [#37010]
* Fixed: Umlauts in project name break exports [#37014]
* Fixed: Setting sync users in synchronized group does not work without on-the-fly LDAP [#37036]
* Fixed: BIM seeder overrides custom design [#37037]

[3.10.0]
* Update OpenProject to 11.3.0
* [Changelog](https://github.com/opf/openproject/releases/tag/v11.3.0)

[3.10.1]
* Update OpenProject to 11.3.1
* Fixed: Search autocompleter n+1 loads schemas -> slow [#34884]
* Fixed: "Click here to open [...] in fullscreenview" not working [#37555]
* Fixed: Work package hierarchy breadcrumb links not working [#37575]
* Fixed: UIM: Not enough space in drop down when user already member [#37578]
* Fixed: Error "Identifier is invalid" when creating a project that starts with a digit [#37583]

[3.10.2]
* Update OpenProject to 11.3.2
* Fixed: Not possible to create or edit projects if relative url root configured [#37571]
* Fixed: Internal server error on custom fields view when using Slovene language [#37607]
* Fixed: Not possible to invite users via modal if relative url root configured [#37618]

[3.10.3]
* Update OpenProject to 11.3.3
* Fixed CVE-2021-32763: Regular Expression Denial of Service in OpenProject forum messages
* Fixed CVE-2021-36390: Host Header Injection in unproxied Docker installations

[3.10.4]
* Update OpenProject to 11.3.4

[3.10.5]
* Update OpenProject to 11.3.5
* Fixed: User email validation regular expression insufficient [#38325]
* Fixed: Inherited role memberhips are not cleaned up if user is removed from a group via LDAP sync [#38614]
* Fixed: Release notes for 11-3-4 is empty [#38678]

[3.11.0]
* Update OpenProject to 11.4.0
* [Release announcement](https://community.openproject.com/versions/1485)
* [Changelog](https://github.com/opf/openproject/releases/tag/v11.4.0)

[3.11.1]
* Update OpenProject to 11.4.1
* [Release announcement](https://community.openproject.com/versions/1491)

[3.12.0]
* Update OpenProject to 12.0.0
* [Release announcement](https://community.openproject.com/versions/1478)

[3.12.1]
* Update OpenProject to 12.0.1
* Fixed: Getting 500 internal server error while clicking the project meeting module [#39853]
* Fixed: Members menu in wrong place (shown above wiki pages) [#39857]
* Fixed: Error message shows every letter as bullet point [#39880]
* Fixed: Updating IFC Models fails. [#39901]

[3.12.2]
* Update OpenProject to 12.0.2
* [Release announcement](https://community.openproject.com/versions/1496)
* Fixed: Getting 500 internal server error while clicking the project meeting module [#39853]
* Fixed: Members menu in wrong place (shown above wiki pages) [#39857]
* Fixed: Error message shows every letter as bullet point [#39880]
* Fixed: Updating IFC Models fails. [#39901]

[3.12.3]
* Only explicitly disable password rest UI for LDAP case.

[3.12.4]
* Update OpenProject to 12.0.3
* [Release announcement](https://community.openproject.com/versions/1498)
* Fixed: Wiki Link List in content formatting issue [#40016]
* Fixed: Translation error when changing permissions [#40056]
* Fixed: WP is not disabled in the background when a modal is open [#40141]
* Fixed: Dropdown menu empty when adding new element to parent child board [#40154]
* Fixed: Optional header sso does not log in plain user [#40240]
* Fixed: Auth source SSO redirects to my page on login [#40248]

[3.12.5]
* Update OpenProject to 12.0.4
* [Release announcement](https://community.openproject.com/versions/1502)

[3.12.6]
* Update OpenProject to 12.0.5
* [Release announcement](https://community.openproject.com/versions/1503)

[3.12.7]
* Update OpenProject to 12.0.6
* [Release announcement](https://community.openproject.com/versions/1504)

[3.12.8]
* Update OpenProject to 12.0.7
* [Release announcement](https://community.openproject.com/versions/1506)

[3.12.9]
* Update OpenProject to 12.0.8
* [Release announcement](https://community.openproject.com/versions/1507)

[3.12.10]
* Update OpenProject to 12.0.9
* [Release announcement](https://community.openproject.com/versions/1511)

[3.12.11]
* Update OpenProject to 12.0.10
* [Release announcement](https://community.openproject.com/versions/1512)
* Fixed: Several Translation Errors [DE] [#41414]
* Fixed: LOCALE env used for seeding is too generic [#41427]
* Fixed: Broken translation in russian/german language after upgrade to actual release [#41434]

[3.13.0]
* Update OpenProject to 12.1.0
* [Release announcement](https://community.openproject.com/versions/1493)

[3.13.1]
* Update OpenProject to 12.1.1
* [Release announcement](https://community.openproject.org/versions/1548)
* Bug #42196: Click on Include projects filter is flickering
* Bug #42209: Unprefixed E-Mail settings ignored
* Bug #42215: Crowdin downloaded translation strings do not match branch
* Bug #42217: Problems by using the timesheet
* Bug #42219: Zero added to previous rates when adding a rate in the rate history with Spanish language selected
* Bug #42225: Inconsistent scrollbars in Include project modal

[3.13.2]
* Update OpenProject to 12.1.2
* [Release announcement](https://community.openproject.org/versions/1549)
* Fixed: Audio of OpenProject video is played when opening the export project modal [#41924]
* Fixed: Wrong row break in Include Project modal for German [#42224]
* Fixed: Work package views created in projects are shown on global work package page [#42249]
* Fixed: Search field does not open on mobile [#42250]
* Fixed: Audio plays when I want to export a wiki page [#42252]
* Fixed: Project filter is reset to "Include all sub-projects" when saving the view [#42277]
* Fixed: Global Basic Auth not working anymore [#42284]
* Fixed: LDAP User Synchronisation doesn't work [#42312]

[3.13.3]
* Update OpenProject to 12.1.3
* [Release announcement](https://community.openproject.org/versions/1550)
* Fixed: "openproject configure" reports errors [#42349]
* Fixed: Scheduled LDAP User Synchronisation doesn't work [#42351]
* Fixed: [Packager] configure fails when sendmail was configured for emails [#42352]

[3.13.4]
* Update OpenProject to 12.1.4
* [Release announcement](https://community.openproject.org/versions/1551)
* Fixed: Removal of the new SPOT buttons because of consistency [#42251]
* Fixed: Falsche Projektzuordnung im Teamplaner [#42271]
* Fixed: Create form crashes when inviting placeholder user into "assigned to" or "responsible" [#42348]
* Fixed: openproject configure fails Ubuntu 20.04 [#42375]
* Fixed: Project clone with global basic auth not working [#42377]
* Fixed: Project include modal doesn't close when clicking the create button [#42380]
* Fixed: Installation packages broken for centos 7 [#42384]
* Fixed: starttls_auto forced to true since Ruby 3 upgrade [#42385]
* Fixed: LDAP user synchronization - administrator flag is overwritten [#42396]
* Fixed: Project filter is not applied in embedded table [#42397]

[3.13.5]
* Update OpenProject to 12.1.5
* [Release announcement](https://community.openproject.org/versions/1552)
* Fixed: Order of work packages in XLS, PDF, CSV export differs from order in OpenProject [#34971]
* Fixed: API notification error on work package query page [#40506]
* Fixed: Custom fields in project settings not sorted [#41074]
* Fixed: openproject configure removes configuration file(s) [#41292]
* Fixed: Special Characters in Comments not "translated" in Activity View [#42194]
* Fixed: Problems by using the timesheet [#42217]
* Fixed: "hostname ... does not match the server certificate" after Update to 12.1.1 [#42304]
* Fixed: Form configuration is accessible without EE token [#42398]
* Fixed: "Project" is not translated in invite user modal [#42479]
* Fixed: Provide a way to disable user status changes from LDAP user sync [#42485]
* Fixed: Permission to avoid editing project overview page not working [#42732]
* Fixed: SSO logout URL is ignored since upgrade to OpenProject 12 [#42735]
* Fixed: Dutch translation additional project status on the project overview page [#42754]
* Fixed: OpenID Connect providers can no longer be set via env [#42810]

[3.13.6]
* Update OpenProject to 12.1.6
* [Release announcement](https://community.openproject.org/versions/1586)
* Bug #42806: EE Trial creation not possible from form configuration
* closedBug #42882: Deleting a user only locks the user
* closedBug #42883: Filtering of work packages does not work any longer by ID
* closedBug #42886: Danger zone: Spaces are collapsed into one in the expected value
* closedBug #42949: Session authentication with warden not triggered
* closedBug #42962: Invalid SMTP configuration breaks project copy
* closedBug #43034: Include projects mistake
* closedBug #43086: dots of Ü are missing in notification list
* closedBug #43138: Included projects not usable for non-admins
* Milestone #42813: Release deployed on Cloud trials
* Milestone #42814: Release deployed on Cloud production
* Milestone #42815: Release deployed on BIM-trials
* closedMilestone #42817: Release provided to Univention app center
* closedMilestone #42825: QA completed
* closedMilestone #42827: Website deployed to include patch release notes

[3.14.0]
* Update OpenProject to 12.2.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.2.0)
* File management with Nextcloud
* Contextual information and warnings when scheduling work packages
* Log time for other users
* Improved navigation bar
* Mark notifications as read outside of Notification Center

[3.14.1]
* Update OpenProject to 12.2.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.2.1)
* Important bug fix for activity records

[3.14.2]
* Update OpenProject to 12.2.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.2.2)
* Fixed: Wrong link for "Documents added" email notification [#41114]
* Fixed: Bulk copy error when Assignee value set 'nobody' [#43145]
* Fixed: Impossible to deselect all days when choosing days to receive mail reminders [#43158]
* Fixed: Error message "Assigned to invalid" syntactically wrong [#43514]
* Fixed: Graphics bugs on mobile [#43555]
* Fixed: New project selector does not work correctly [#43720]
* Fixed: Error 500 (undefined method `getutc') when opening activities [#43771]
* Fixed: Wiki unacessible if last menu item got lost [#43841]
* Fixed: Unable to set LDAP filter string through rake task ldap:register [#43848]
* Fixed: Upgrade 12.1.4 to 12.2.1 fails: pending database migration [#43876]
* Fixed: System 'read only' field [#43893]

[3.14.3]
* Update OpenProject to 12.2.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.2.3)
* Fixed: Installing custom plugins in packaged installations

[3.14.4]
* Update OpenProject to 12.2.4
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.2.4)
* Fixed: Remaining hours sum not well formed [#43833]
* Fixed: Destroy journals with invalid data_type associations [#44132]
* Fixed: Internal error / Illegal instruction error [#44155]
* Fixed: Dragging images to CKEditor on Grid custom texts not working with direct upload [#44156]

[3.14.5]
* Update OpenProject to 12.2.5
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.2.5)
* Fix LDAP group synchronization bug

[3.15.0]
* Update OpenProject to 12.3.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.3.0)

[3.15.1]
* Update OpenProject to 12.3.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.3.2)
* Fixed: Multiples Identicals Webhooks are sent for each WP change applied, not considering the Aggregated WorkPackage Journal [#44158]
* Fixed: Moving a week-days-only WP on Gantt chart and falling its end-date to a non-working date is not possible [#44501]
* Fixed: Migration to 12.3.1 fails with Key columns "user_id" and "id" are of incompatible types: numeric and bigint. [#44634]
* Fixed: rake assets:precompile fails with NameError: uninitialized constant ActiveRecord::ConnectionAdapters::PostgreSQLAdapter [#44635]

[3.15.2]
* Update OpenProject to 12.3.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.3.3)
* Fixed: Gantt calendar weeks not according to settings [#41327]
* Fixed: BIM edition unavailable on Ubuntu 22.04 packaged installation [#43531]
* Fixed: Move workpackage in other project error [#43553]
* Fixed: Unable to set User Default Timezone in configuration.yml [#44737]
* Fixed: Initial Installation fails if welcome_text is set in configuration.yml [#44755]
* Fixed: OAuth login POST doesn't work on mobile Safari due to CSP [#44772]

[3.15.3]
* Update OpenProject to 12.3.4
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.3.4)
* Fixed: Chrome v108 freezes up on work package create and show page [#45169]

[3.16.0]
* Update OpenProject to 12.4.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.0)
* Date alerts for the upcoming dates and overdue tasks
* Work week for the team planner
* List up to three people in a notification card
* Self notifications via @mentioned
* Bulk edit work package "subject" field
* Many bugfixes

[3.16.1]
* Update OpenProject to 12.4.1
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.1)
* Fixed: Margin under road map caption when version is overdue [#41676]
* Fixed: Issues with project selection on mobile [#44440]
* Fixed: Typo in translation source string [#45071]
* Fixed: Can not sort project CF in Administration [#45099]
* Fixed: Centos7 fails with "undefined symbol: PQconninfo" due to outdated postgresql-libs shared library [#45101]
* Fixed: Labels are not fully visible for date alerts in DE, FR, ES [#45163]
* Fixed: Work package wrongfully displayed as child candidate [#45171]
* Fixed: Member shown twice when filtering by group when in multiple groups [#45331]
* Fixed: Custom Fields in WP View (Full and Side) are scrubbed [#45343]
* Fixed: In Activity page, "subprojects" is checked when navigating between pages [#45348]
* Fixed: Can not delete files from WiKi pages [#45385]
* Fixed: Missing Translation for duration activity changes [#45391]
* Fixed: Bad Spacing between versions in Roadmap page [#45392]
* Fixed: Login and email missing during AzureAD OpenID Connect registration [#45415]
* Fixed: xeokit-metadata fails to find dotnet-runtime in Ubuntu 22.04 [#45442]

[3.16.2]
* Update OpenProject to 12.4.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.2)
* Support custom env variables in /app/data/env
* Fixed: Password confirmation dialog doesn't work when using ENTER instead of clicking on Confirm [#44499]
* Fixed: File picker does not load directory listing when OAuth token is expired [#44916]
* Fixed: Activated checkbox in PDF export [#44940]
* Fixed: Change "Log time" step to 0.25h instead of 0.01h [#45091]
* Fixed: dots of Ü are missing in search autocomplete (Umlauts) [#45218]
* Fixed: Copying projects slow if groups with a lot of users are members in a lot of projects [#45224]
* Fixed: Help menu entries overlapping for non-English language settings [#45434]
* Fixed: Messy rendering of webhook show page [#45438]
* Fixed: Spacing issue in date alert dropdown [#45443]
* Fixed: Attributes that span both columns have too much spacing between label and input [#45445]
* Fixed: The finish date slider for a task via the gannt chart is not accurate [#45536]
* Fixed: Date alert creation job timing out [#45591]
* Fixed: New custom field with default value breaks comments [#45724]

[3.16.3]
* Update OpenProject to 12.4.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.2)
* Support custom env variables in /app/data/env
* Fixed: Password confirmation dialog doesn't work when using ENTER instead of clicking on Confirm [#44499]
* Fixed: File picker does not load directory listing when OAuth token is expired [#44916]
* Fixed: Activated checkbox in PDF export [#44940]
* Fixed: Change "Log time" step to 0.25h instead of 0.01h [#45091]
* Fixed: dots of Ü are missing in search autocomplete (Umlauts) [#45218]
* Fixed: Copying projects slow if groups with a lot of users are members in a lot of projects [#45224]
* Fixed: Help menu entries overlapping for non-English language settings [#45434]
* Fixed: Messy rendering of webhook show page [#45438]
* Fixed: Spacing issue in date alert dropdown [#45443]
* Fixed: Attributes that span both columns have too much spacing between label and input [#45445]
* Fixed: The finish date slider for a task via the gannt chart is not accurate [#45536]
* Fixed: Date alert creation job timing out [#45591]
* Fixed: New custom field with default value breaks comments [#45724]

[3.16.4]
* Update OpenProject to 12.4.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.3)
* Fixed: Custom field in cost report show "not found" after custom filed's value [#34978]
* Fixed: Race condition with outdated OAuth access token [#45799]
* Fixed: Notifications API are still showing errors in case of not authorized / other errors [#45854]
* Fixed: Administration: Permissions report page doesn't work in french in 12.4.1 [#45892]

[3.16.5]
* Update OpenProject to 12.4.4
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.4)
* Fixed: Deleting tmp/cache works when called manually but never by scheduled jobs [#44182]
* Fixed: Not possible to delete favicon and touch icon [#45997]
* Fixed: Sendmail not working in 12.4.3 [#46152]
* Fixed: Prevent OAuth refresh token race condition. [#46195]
* Fixed: Click started in modal, but dragged outside closes the modal [#46217]

[3.16.6]
* Update OpenProject to 12.4.5
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.4.5)
* Fixed: Timeout when bulk editing work package assignees across projects [#46284]
* Fixed: Groups can no longer have their notifications suppressed [#46330]

[3.17.0]
* Update OpenProject to 12.5.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.0)

[3.17.1]
* Update OpenProject to 12.5.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.1)
* Fixed: Changing non working days in Polish fails [#47020]
* Fixed: Unable to login via oauth provider (e.g. Azure) [#47044]

[3.17.2]
* Update OpenProject to 12.5.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.2)
* Fixed: [AppSignal] incompatible character encodings: ASCII-8BIT and UTF-8 [#43898]
* Fixed: Missing deletion confirmation for subprojects [#45935]
* Fixed: Green button turns to black on hover [#47026]
* Fixed: Time log entries too coarse [#47027]
* Fixed: Burndown charts empty since 12.5 [#47079]
* Fixed: Direct download of a storage file fails [#47113]
* Fixed: Fix direct uploads when Nextcloud configured without pretty URLs [#47152]
* Fixed: Swagger UI is not rendering for API docs [#47157]
* Changed: Add hint if Nextcloud App "OpenProject Integration" needs upgrade for 12.5 [#47021]

[3.17.3]
* Update OpenProject to 12.5.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.3)
* Fixed: Titles of related work packages are unecessarily truncated. Full titles are not accessible. [#44828]
* Fixed: Date picker: selected dates in mini calendar don't have a hover (primary dark) [#46436]
* Fixed: Non-working Days/Holidays selection with 12.5 update [#47057]
* Fixed: Project filter values drop down cut off [#47072]
* Fixed: In projects filter selected values keep being suggested [#47074]
* Fixed: Acitivity page not working correctly [#47203]
* Fixed: XLS export of work package with description cannot be opened by Excel if the description contains a table [#47513]
* Fixed: Cannot archive a project that has archived sub-projects [#47599]
* Fixed: 'TypeError: can't cast Array' during db:migrate [#47620]
* Fixed: Anyone can sign up using Google even if user registration is disabled [#47622]
* Fixed: inbound emails uses "move_on_success" and "move_on_failure" error [#47633]

[3.17.4]
* Update OpenProject to 12.5.4
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.4)
* Invalidation of existing sessions when 2FA activated [#48035]
* Invalidation of password reset link when user changes password in the meantime [#48036]
* Fixed: Google reCAPTCHA v2 and V3 changed implementation [#44115]
* Fixed: User activity: Previous link removes user parameter from URL [#47855]
* Fixed: Work package HTML titles needlessly truncated [#47876]
* Fixed: Wrong spacing in Firefox when using line breaks in user content tables [#48027]
* Fixed: Previously Created Session Continue Being Valid After 2FA Activation [#48035]
* Fixed: Forgotten password link does not expire when user changes password in the meantime [#48036]

[3.17.5]
* Fix email invitation sending

[3.17.6]
* Update OpenProject to 12.5.5
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.5)
* Fixed: API v3 Group List Api sometimes misses embedded members field [#42303]
* Fixed: Wrong date format for the Slovenian language [#48032]
* Fixed: MyProjectPageToGrid migration fails [#48122]
* Fixed: Missing translation for "Comment added" on work package activity tracking [#48157]
* Fixed: Links from the welcome text stop working when text is edited [#48158]
* Fixed: Document not listing project name under My Page [#48177]

[3.17.7]
* Update OpenProject to 12.5.6
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.6)
* CVE-2023-31140: Project identifier information leakage through robots.txt
* Changed: Add packaged installation support for SLES 15 [#44117]
* Changed: Allow URL behind the application logo to be configurable [#48251]
* Fixed: Moving in Kanban board having a "is not" project filter changes the project of the work packages [#44895]
* Fixed: Upgrade migration error `"smtp_openssl_verify_mode is not writable"` [#48125]

[3.17.8]
* Update OpenProject to 12.5.7
* Update Phusion Passenger to 6.0.18
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.7)
* Changed: Quick-Wins to make blue boxes easier to understand [#44340]
* Fixed: Milestone cannot be dragged left-right on the calendar [#48334]
* Fixed: Docker linux/arm64 image raise "/app/docker/prod/gosu: cannot execute binary file: Exec format error" [#48395]

[3.17.9]
* Update OpenProject to 12.5.8
* [Full changelog](https://github.com/opf/openproject/releases/tag/v12.5.8)
* Fixed: After calling "Create project copy" API endpoint, the Job Status API should return the new projects id, not only its identifier [#37783]
* Fixed: Entries in summary emails not clickable in Outlook (links not working) [#40157]
* Fixed: Custom project attribute triggers error when selected during project creation [#46827]
* Fixed: Preview of linked WP is cut off in split view when close to the edge [#46837]
* Fixed: Selected date (number) not visible when matches with Today [#47145]
* Fixed: Opening of CKEditor sporadicallly taking 10s+ when trying to comment on work packages [#47795]
* Fixed: Projects tab of group administration should not offer adding to archived projects [#48263]
* Fixed: Grape responds with text/plain 303 redirect to a JSON api [#48622]
* Fixed: Meeting Minutes double submit causes lock version error [#49061]
* Fixed: Cost reports XLS export results in timeout of web request [#49083]
* Fixed: Internal error occurs when invalid project is set to template [#49116]
* Changed: Allow internal login even if omniauth direct provider selected [#47930]

[3.18.0]
* Update OpenProject to 13.0.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.0)
* Multi-select custom fields for the Community
* Time tracking button
* Global modules are listed on new index pages
* Subscribe to calendars via URL (iCalendar format)
* PDF Export to create up-to-date and accurately formatted work plans
* Allow localized seeding of all seed data
* Show recently viewed work packages in the global search
* Add shortcut to “My activity” link in the account menu
* Enable “Copy link to clipboard” for work packages
* Enlarge the scroll handle for the work package sidebar
* Enable double click on notifications to open details view
* Add Emoji picker to the text editor
* Introduce Primer Design System

[3.18.1]
* Update OpenProject to 13.0.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.1)
* Changed: Forbid user to enable misconfigured storages for a project. [#49218]
* Fixed: Copy to clipboard icon wrong [#49721]
* Fixed: Shared calendar return 404 when using a parent filter [#49726]
* Fixed: Identity providers sign in icons overlap with the text or not enough spacing between them [#49749]
* Fixed: Not enough spacing between the information box and the heading on LDAP page [#49752]
* Fixed: Required custom field list with default value prevents comments [#49765]
* Fixed: Error during upgrade to 13.0.0 [#49771]
* Fixed: Blank page after login on iphone [#49774]
* Fixed: Error on creating version board (with multiple versions) [#49782]
* Fixed: CF multiselect list with default values does not show default values on work package creation [#49784]
* Fixed: Chinese zh-CN locale is not up to date [#49795]

[3.18.2]
* Update OpenProject to 13.0.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.2)

[3.18.3]
* Update OpenProject to 13.0.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.3)
* Fixed: Enterprise Trial automatic activation does not work [#49781]
* Fixed: Fix untranslated strings [#49848]
* Fixed: Layout of work package status and type selectors are broken once a status or type was changed [#49858]
* Fixed: Inconsistent pluralization of global permissions and wrong capitalization of "Global Role" [#49989]
* Fixed: 500 error when creating a news title longer than 60 characters [#50020]

[3.18.4]
* Update OpenProject to 13.0.4
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.4)
* Fixed: PDF export does not contain custom fields and some core values [#49948]
* Fixed: Poor performance of work package autocompleter and /api/v3/work_packages in some scenarios [#50102]
* Fixed: Poor performance of /api/v3/time_entries [#50130]
* Fixed: Poor performance of mentions autocompleter [#50144]
* Fixed: DATABASE_URL environment variable is not validated before usage [#50152]
* Fixed: Work package activity not shown when using Polish language [#50197]

[3.18.5]
* Update OpenProject to 13.0.5
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.5)
* Fixed: get 500 error when open wiki page in every project [#40500]
* Fixed: [AppSignal] Performance VersionsController#index [#47872]
* Fixed: File Drag and Drop [#49507]
* Fixed: Improve discoverablility of the "Permissions report" [#50106]
* Fixed: Inconsistent labels for "Global roles" and "Project" in user tab [#50108]

[3.18.6]
* Update OpenProject to 13.0.6
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.6)
* Fixed: Project storage members page can be accessed without a session. [#50519]

[3.18.7]
* Update OpenProject to 13.0.7
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.7)
* Fixed: File Drag and Drop [#49507]
* Fixed: Help icon not shown when having a custom help link setting [#50666]

[3.18.8]
* Update OpenProject to 13.0.8
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.0.8)
* Fixed: Logging time using "My spent time" widget returns previous day (likely time zone issue) [#49779]
* Fixed: Internal server error upon login via Microsoft Entra ID (AzureAD) [#50167]
* Fixed: IFC conversion fails (libhostfxr.so not found) (reintroduced bug) [#50172]
* Fixed: +Custom field button moved left (instead of being on the right) of the Custom fields page in Project settings [#50285]
* Fixed: Make Nextcloud synchronization more stable in 13.0. [#51265]

[3.19.0]
* Update OpenProject to 13.1.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.1.0)

[3.19.1]
* Update OpenProject to 13.1.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.1.1)
* Fixed: Inconsistent hrefs in wp shared mail [#51480]
* Fixed: Slow notification polling [#51622]
* Fixed: Error from rails at openproject configure : Rails couldn't infer whether you are using multiple databases from your database.yml and can't generate the tasks for the non-primary databases. [#51625]
* Fixed: Share modal has two close options [#51652]
* Fixed: Missing translation " wrote:" when quoting in work package activity [#51656]
* Fixed: Very slow Project.visible scope for administrators [#51659]
* Fixed: Meeting agenda create Form Buttons overlap on mobile [#51687]
* Fixed: Time and costs [#51700]
* Fixed: Pasting into autocompleter does not work initially [#51730]

[3.19.2]
* Update OpenProject to 13.1.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.1.2)
* Fixed: Unnecessary bullet displayed for TODO checkboxes in wp comments [#45469]
* Fixed: Black text invisible on some darker colors in the Task modal of the Task board [#49934]
* Fixed: White space at the end of page when scrolling a wiki page [#51680]
* Fixed: Migration from 12.x to 13.1 fails for installations already having file links in the database [#51801]

[3.20.0]
* Update OpenProject to 13.2.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.2.0)
* Show file links of files that are not available to the user in the cloud storage
* Filter for roles, groups, and shares in the project members list
* Status change without rights to edit a work package
* Quick context menu in Gantt view: Show relations
* New field names and calculation of work and remaining work

[3.20.1]
* Update OpenProject to 13.2.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.2.1)
* Bugfix: Underscore is treated as wildcard in search filter [#33574]
* Bugfix: Custom actions buttons look inactive until we hover over them and click [#45677]
* Bugfix: Remove custom field from all type configurations leaves them active in project, shown in bulk edit [#49619]
* Bugfix: Columns in task board not in sync for more than one task (column width not working) [#49788]
* Bugfix: IFC conversion fails (libhostfxr.so not found) (reintroduced bug) [#50172]
* Bugfix: Please refrain from overwriting logrotate settings with every single update [#50477]
* Bugfix: Work packages get lost when Teamplanner's time frame switch from Work week to 2 weeks [#50895]
* Bugfix: Can't pay for the Subscription after my trial period has ended [#51230]
* Bugfix: Checkboxes are not correclty displayed in the CkEditor [#51247]
* Bugfix: Error 500 when trying to view a budget with a running WP timer [#51460]
* Bugfix: /opt/openproject/lib/redmine/imap.rb:53:in `new': DEPRECATED: Call Net::IMAP.new with keyword options (StructuredWarnings::StandardWarning) [#51799]
* Bugfix: Renaming Work Package Views/ Boards : Edit Lock Issue [#51851]
* Bugfix: Date is not correct on the boards cards due to time zone difference [#51858]
* Bugfix: Enterprise icon is inconsistently aligned in the sidebar [#52097]
* Bugfix: Files tab shows bad error message on request timeout to remote storage [#52181]
* Bugfix: OIDC backchannel logout broken as retained session values are not available in the user_logged_in_hook [#52185]
* Bugfix: PDF export fails with "undefined method `sourcepos'" [#52193]
* Bugfix: Roadmap progress is overflowing [#52232]
* Bugfix: Work package "+ Create" button drop down only opening every second time [#52260]
* Bugfix: Creating Work Package - Mentions not working anymore [#52298]
* Bugfix: remaining hours cropped on task board view [#52362]
* Bugfix: Work packages: Create child fails if milestone is first selected type [#52373]
* Bugfix: Copying project fails when work package with children is copied [#52384]
* Bugfix: Dynamic meeting HTML titles missing [#52389]

[3.21.0]
* Update OpenProject to 13.3.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.3.0)

[3.21.1]
* Update OpenProject to 13.3.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.3.1)
* Bugfix: Work package status % is not added up for parents [#33961]
* Bugfix: Consent always shown in Browser language despite user's language being set differently [#52287]
* Bugfix: Dynamics meetings: improve readability of agenda items (quote vs. normal text) [#52519]
* Bugfix: Ckeditor deletes editor content on firefox [#52641]
* Bugfix: Work package title not readable on smaller screens [#52671]
* Bugfix: Queries are not highlighted in the menu [#52758]
* Bugfix: When multi select user custom fields are truncated, the associated tooltip contains incorrect data. [#52765]
* Bugfix: Migration fails when default language is pt-BR [#52805]
* Bugfix: Onboarding tour displays permission error [#52826]
* Bugfix: Seeding the BIM edition in a development environment fails [#52906]
* Bugfix: Storages automatic project folders is exhausting file descriptors on workers [#52928]
* Bugfix: Users who booked unit costs cannot be deleted [#53038]

[3.22.0]
* Update OpenProject to 13.4.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.4.0)
* GitLab integration (originally developed by Community contributors)
* Advanced features for custom project lists
* Advanced features for the Meetings module
* Virus scanning functionality with ClamAV
* Intermediate loading modal during the OAuth nudge
* PDF Export: Lists in table cells are supported
* WebAuthn/FIDO/U2F is added as a second factor
* More languages added to the default available set

[3.22.1]
* Update OpenProject to 13.4.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v13.4.1)
* Bugfix: Error 500 on project lists after upgrade to 13.4.0 [#53570]
* Bugfix: PT-BR translation works as PT-PT [#53584]
* Bugfix: Work Package Table crashes when grouped by integer and sums are displayed [#53609]
* Bugfix: SettingSeeder rake aborted! when upgrade from 13.3.0 to 13.4.0 [#53611]
* Bugfix: Internal error (comparisson NilClass with String failed) on query 5000 on community [#53617]

[3.23.0]
* Update OpenProject to 14.0.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.0.0)

[3.23.1]
* Update OpenProject to 14.0.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.0.1)
* Bugfix: uninitialized constant CostQuery::Filter::CustomField [#54500]
* Bugfix: openDesk setup script fails [#54558]
* Bugfix: Totals of non-parent work packages are updated when status is changed in administration [#54646]
* Bugfix: User receives email updates even if their account is permanently locked [#54714]
* Feature: Snappier progress popover [#54464]

[3.24.0]
* Update OpenProject to 14.1.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.1.0)
* Bugfix: Improper escaping of custom field values in cost report [#55198]
* PDF export of Gantt view, e.g. for printing (Enterprise add-on)
* Favorite projects
* Advanced features for the Meetings module
* Possibility to hide attachments in the Files tab
* File storages module activated by default
* Custom fields of the type Link (URL)
* Save a changed sort order of custom project lists
* A "Manage project attributes" button on the project overview page
* OneDrive/SharePoint: A "no permission“ message to file links

[3.24.1]
* Update OpenProject to 14.1.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.1.1)
* Bugfix: Waiting modal doesn't time out [#55016]
* Bugfix: ScheduleReminderMailsNotificationsJob constantly broken after 24 hour outage - no reminder mails send [#55223]
* Bugfix: The `available_projects` api endpoint displays custom project fields incorrectly [#55381]
* Bugfix: Typo in historic migation [#55421]

[3.25.0]
* Update OpenProject to 14.2.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.2.0)
* Display Work and Remaining work in days and hours
* Exclude by status some work packages from the calculation of totals for % Complete and work estimates
* Warn admins about potential data loss when changing progress calculation modes
* Configure which projects are activated for a project attribute
* Avoid redundant emails in case of @mentions and email reminder
* Allow renaming persisted project lists
* Allow meeting invite to be sent out when creating meetings
* Embedded work package attributes in PDF export

[3.25.1]
* Update OpenProject to 14.2.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.2.1)

[3.26.0]
* Update OpenProject to 14.3.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.3.0)
* Move from apache passenger to puma webserver
* Add missing worker for faster task queue processing

[3.27.0]
* Update OpenProject to 14.4.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.4.0)

[3.27.1]
* Update OpenProject to 14.4.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.4.1)
* Requires Cloudron 8.0.0

[3.27.2]
* Update OpenProject to 14.4.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.4.2)
* Bugfix: Docker: JavaScript isn't minimized [#57559]
* Bugfix: Docker slim version got pushed as all-in-one container [#57561]

[3.28.0]
* Update OpenProject to 14.5.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.5.0)
* Keep and restore local versions of WYSIWYG content changes
* Enable a storage for multiple projects at once
* Export work package lists in an updated modal with additional settings
* Updated user experience in the notification center
* Release to Community: Display custom field columns/filters in project lists

[3.28.1]
* Update OpenProject to 14.5.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.5.1)
* Bugfix: Internal server error opening budget [#57905]
* Bugfix: User can't create a new global role [#57906]
* Bugfix: German translations not complete [#57908]
* Bugfix: User can't be removed from global role [#57928]
* Bugfix: Incorrect read-only label for SSO logins [#57961]
* Bugfix: The Project Settings-Information page does not load [#57981]
* Bugfix: Bump ruby-saml to address CVE-2024-45409 [#57984]
* Bugfix: Cannot delete users who created meeting agenda items [#57986]

[3.29.0]
* Update OpenProject to 14.6.0
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.6.0)
* Updated progress reporting with most flexible options
* Meetings: Receive an update notification with reload option
* Enable and disable a custom field for multiple projects at once
* Use relative work package attribute macros
* Show empty lines in saved rich textSee past meetings included in the number next to the Meetings tab
* Changes to design settings in administration

[3.29.1]
* Update OpenProject to 14.6.1
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.6.1)
* Bugfix: Side menu for work package lists not displayed [#​57663]

[3.29.2]
* Update OpenProject to 14.6.2
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.6.2)
* Bugfix: Meetings: Invitation being sent out instead of summary after meeting took place [#​58326]
* Bugfix: Some images are missing when running OpenProject all-in-one container [#​58431]
* Bugfix: Deleting cost query and clicking apply again breaks [#​58441]

[3.29.3]
* Update OpenProject to 14.6.3
* [Full changelog](https://github.com/opf/openproject/releases/tag/v14.6.3)
* Bugfix: Reminder mail not sent in case more than 15 notifications are to be reported about [#58555]
* Bugfix: Custom field filters removed when activating baseline [#58600]

[3.30.0]
* Update openproject to 15.0.1
* [Full Changelog](https://github.com/opf/openproject/releases/tag/v15.0.1)
* Bugfix: OpenID Connect Microsoft Entra: Tenant not correctly output \[[#&#8203;59261](https://community.openproject.org/wp/59261)]
* [Change the design of the Activity panel to Primer](https://community.openproject.org/wp/54733)
* [Emoji reactions to work package comments](https://community.openproject.org/wp/40437)
* [Continuously update the notification center. Don't ask for loading updates.](https://community.openproject.org/wp/58253)
* [Remove "Latest activity" section from work package "Overview" tab](https://community.openproject.org/wp/58017)
* [On "Newest at the bottom" have the line touch the input box](https://community.openproject.org/wp/57262)
* The comment box being a fixed element anchored to the bottom of the split screen area.
* Filtering the Activity panel with options to either show everything, changes only or comments only.
* Ordering to either newest on top or newest at the bottom.
* [User interface for OIDC (OpenID Connect) provider configuration](https://community.openproject.org/wp/57677)
* [User interface for SAML configuration](https://community.openproject.org/wp/40279)
* [Show danger zone when deleting SAML or OIDC provider](https://community.openproject.org/wp/58451)
* [Allow setting omniauth_direct_login_provider through UI](https://community.openproject.org/wp/58437)

[3.30.1]
* Update openproject to 15.0.2
* [Full Changelog](https://community.openproject.org/wp/54733)
* Bugfix: Work package creation event not displayed if aggregation includes a comment \[[#&#8203;58738](https://community.openproject.org/wp/58738)]
* Bugfix: Error toast giving a 500 error without relevant details \[[#&#8203;59065](https://community.openproject.org/wp/59065)]
* Bugfix: The comment box remains open and does not blur on submitting comment \[[#&#8203;59279](https://community.openproject.org/wp/59279)]
* Bugfix: Split screen activity tab does not scroll to the right position from list view \[[#&#8203;59281](https://community.openproject.org/wp/59281)]
* Bugfix: Storage in copied project does not have the correct Project Folder setting \[[#&#8203;59344](https://community.openproject.org/wp/59344)]
* Bugfix: Cannot change limit_self_registration for SAML auth providers \[[#&#8203;59370](https://community.openproject.org/wp/59370)]
* Bugfix: Option to limit_self_registration in SAML provider not respected \[[#&#8203;59375](https://community.openproject.org/wp/59375)]
* Bugfix: Migration of SAML auth providers doesn't retain limit_self_registration \[[#&#8203;59403](https://community.openproject.org/wp/59403)]
* Bugfix: OpenID connect does not allow setting custom scopes \[[#&#8203;59430](https://community.openproject.org/wp/59430)]

[3.31.0]
* Update openproject to 15.1.0
* Feature: Multi-level selection fields to support custom hierarchical attributes \[[#&#8203;36033](https://community.openproject.org/wp/36033)]
* Bugfix: Unsorted "User" list in "Time and costs" \[[#&#8203;43829](https://community.openproject.org/wp/43829)]
* Bugfix: 500 when filtering by date field and specifying too big number \[[#&#8203;55393](https://community.openproject.org/wp/55393)]
* Bugfix: Sorting by custom field has strong impact on performance for the project list \[[#&#8203;57305](https://community.openproject.org/wp/57305)]
* Bugfix: Absent value for custom field is ordered not consistently at the beginning or end for different formats \[[#&#8203;57554](https://community.openproject.org/wp/57554)]
* Bugfix: Notification on a mention added to an edited comment is not triggered \[[#&#8203;58007](https://community.openproject.org/wp/58007)]
* Bugfix: Info box on new custom field of type hierarchy is permanent \[[#&#8203;58466](https://community.openproject.org/wp/58466)]
* Bugfix: Item add form disappears after added a new item to a custom field of type hierarchy \[[#&#8203;58467](https://community.openproject.org/wp/58467)]
* Bugfix: Using multi-select and required options do not work \[[#&#8203;58635](https://community.openproject.org/wp/58635)]
* Bugfix: HTML files served as plain text \[[#&#8203;58646](https://community.openproject.org/wp/58646)]
* Bugfix: Performance issues on work_packages api endpoint \[[#&#8203;58689](https://community.openproject.org/wp/58689)]
* Bugfix: Breadcrumb of hierarchy items has left margin \[[#&#8203;58700](https://community.openproject.org/wp/58700)]
* Bugfix: Missing hint in comment box label \[[#&#8203;59060](https://community.openproject.org/wp/59060)]
* Bugfix: Automatic comments to indicate retractions miss a stem (newest on top) \[[#&#8203;59278](https://community.openproject.org/wp/59278)]
* Bugfix: Add local spacing to inline enterprise banner \[[#&#8203;59284](https://community.openproject.org/wp/59284)]
* Bugfix: Hierarchy custom fields causing 500 on custom actions \[[#&#8203;59354](https://community.openproject.org/wp/59354)]
* Bugfix: Files count badge missing in Files tab (WP full view) \[[#&#8203;59391](https://community.openproject.org/wp/59391)]
* Bugfix: Signing in after two factor methods have been deleted lead to a 500 error \[[#&#8203;59408](https://community.openproject.org/wp/59408)]
* Bugfix: User without permission to "Save views" can save changes to work package views \[[#&#8203;59479](https://community.openproject.org/wp/59479)]
* Bugfix: Double provider showing on OpenID provider list \[[#&#8203;59510](https://community.openproject.org/wp/59510)]
* Bugfix: SAML provider not available when migrated with idp_cert_fingerprint \[[#&#8203;59535](https://community.openproject.org/wp/59535)]
* Bugfix: Hierarchy items not correctly displayed if custom field is shown in wp table \[[#&#8203;59572](https://community.openproject.org/wp/59572)]
* Bugfix: Export of an unsaved query not working properly \[[#&#8203;59781](https://community.openproject.org/wp/59781)]
* Bugfix: No comments possible for shared work packages that were shared with "Comment" permission \[[#&#8203;59785](https://community.openproject.org/wp/59785)]
* Bugfix: Missing space between project selector and "include sub-projects"-checkbox \[[#&#8203;59795](https://community.openproject.org/wp/59795)]
* Bugfix: Work package create button doesn't work on mobile Web \[[#&#8203;59828](https://community.openproject.org/wp/59828)]
* Bugfix: Trailing ' in journal diff parameter activity_page breaks page \[[#&#8203;59865](https://community.openproject.org/wp/59865)]
* Bugfix: Regression: Meeting sorted in reverse order \[[#&#8203;59908](https://community.openproject.org/wp/59908)]
* Bugfix: Not possible to update auto-discovered values \[[#&#8203;59928](https://community.openproject.org/wp/59928)]
* Bugfix: Form buttons are left aligned and not right aligned in hierarchy items form \[[#&#8203;59978](https://community.openproject.org/wp/59978)]

[3.31.1]
* Update openproject to 15.1.1
* Bugfix: Hierarchy custom fields: Can circumvent enterprise restrictions \[[#&#8203;59985](https://community.openproject.org/wp/59985)]
* Bugfix: Cannot delete users who have favourite projects \[[#&#8203;60171](https://community.openproject.org/wp/60171)]
* Bugfix: Deletion of users who have created authorization providers (SAML, OIDC) silently fails \[[#&#8203;60172](https://community.openproject.org/wp/60172)]
* Bugfix: Relations tab should not show relations to work packages a user has no read access to \[[#&#8203;60479](https://community.openproject.org/wp/60479)]
* Bugfix: Child relation for milestones must be prevented in relations tab \[[#&#8203;60512](https://community.openproject.org/wp/60512)]

[3.32.0]
* Update openproject to 15.2.0
* [Full Changelog](https://community.openproject.org/wp/54733)
* Feature: Edit lag to follow-precedes-relations \[[#&#8203;22360](https://community.openproject.org/wp/22360)]
* Feature: Create new folder from within the location picker \[[#&#8203;46811](https://community.openproject.org/wp/46811)]
* Feature: Update PageHeaders & SubHeaders in the (rails) project pages (Part 1) \[[#&#8203;53810](https://community.openproject.org/wp/53810)]
* Feature: Popover for user information on hover \[[#&#8203;55581](https://community.openproject.org/wp/55581)]
* Feature: Enable group by CF hierarchy for the work package query \[[#&#8203;59174](https://community.openproject.org/wp/59174)]
* Feature: Basic functionality of work package "Reminders", scheduling "Reminder" notifications \[[#&#8203;59436](https://community.openproject.org/wp/59436)]
* Feature: Enable ordering for work packages on values of custom fields of type hierarchy \[[#&#8203;59766](https://community.openproject.org/wp/59766)]
* Feature: When adding new relations, auto-scroll to show the newly added relation \[[#&#8203;59769](https://community.openproject.org/wp/59769)]
* Feature: Create new icon for setting reminders \[[#&#8203;59793](https://community.openproject.org/wp/59793)]
* Feature: Export cost query as timesheet PDF \[[#&#8203;59824](https://community.openproject.org/wp/59824)]
* Feature: Improve comprehensibility of the "Add relations" modal \[[#&#8203;60462](https://community.openproject.org/wp/60462)]
* Bugfix: Mobile: tap twice on comment input to start typing \[[#&#8203;57107](https://community.openproject.org/wp/57107)]
* Bugfix: Activity panel (Notification center goes into mobile layout even on a desktop when the browser on desktop \[[#&#8203;59235](https://community.openproject.org/wp/59235)]
* Bugfix: Folders missing in log lines for "Unexpected Content Error" \[[#&#8203;59346](https://community.openproject.org/wp/59346)]

[3.32.1]
* Update openproject to 15.2.1
* [Full Changelog](https://community.openproject.org/wp/54733)
* Bugfix: Error on applying filter changes in Cost Reports \[[#&#8203;60023](https://community.openproject.org/wp/60023)]
* Bugfix: Project attributes of type list cannot be edited anymore \[[#&#8203;60388](https://community.openproject.org/wp/60388)]
* Bugfix: Background job to schedule new meeting instances failed \[[#&#8203;60621](https://community.openproject.org/wp/60621)]
* Bugfix: Missing debounce when searching for work packages in relations tab \[[#&#8203;60649](https://community.openproject.org/wp/60649)]
* Bugfix: Slow /api/v3/work_packages/\*/available_relation_candidates \[[#&#8203;60732](https://community.openproject.org/wp/60732)]
* Bugfix: Date alerts blocking more important background jobs \[[#&#8203;60856](https://community.openproject.org/wp/60856)]
* Bugfix: RHEL: Can't install BIM edition \[[#&#8203;60870](https://community.openproject.org/wp/60870)]
* Bugfix: High DB load caused by date alert background jobs \[[#&#8203;60932](https://community.openproject.org/wp/60932)]
* Bugfix: Setting the user display format without a lastname breaks User custom fields with group values \[[#&#8203;60976](https://community.openproject.org/wp/60976)]
* Bugfix: Buttons can't fit on the Activity Tab for Russian and German \[[#&#8203;61053](https://community.openproject.org/wp/61053)]
* Bugfix: Label text in comment box is overflowing in German \[[#&#8203;61088](https://community.openproject.org/wp/61088)]
* Bugfix: HTML injection in device name \[[#&#8203;61089](https://community.openproject.org/wp/61089)]

[3.33.0]
* Update openproject to 15.3.0
* [Full Changelog](https://www.openproject.org/docs/release-notes/15-3-0/)
* **Set up meeting series**: Schedule meetings to recur on a weekly, biweekly, or custom basis.
* **Manage occurrences**: Each meeting in a series is an individual occurrence, allowing adjustments without affecting the entire series.
* **Flexible modifications**: Adjust single occurrences within a recurring meeting series without disrupting the entire schedule.
* **Improved meeting index page**: Recurring meetings are clearly displayed in the meetings index, helping users track their schedules efficiently.
* **ICS file support**: Download and integrate recurring meetings into external calendar applications.
* **Custom meeting templates**: Define standard templates for recurring meetings to ensure consistency.
* **Invitation emails for meeting series**: Participants receive automated notifications for scheduled recurring meetings.

[3.33.1]
* Update openproject to 15.3.1
* [Full Changelog](https://www.openproject.org/docs/release-notes/15-3-0/)
* Bugfix: In add relation dialog, selecting a WP that user doesn't have permission to edit raises an error \[[#&#8203;60858](https://community.openproject.org/wp/60858)]
* Bugfix: Danger and Feedback Dialogs have broken ARIA describedby attribute value \[[#&#8203;61347](https://community.openproject.org/wp/61347)]
* Bugfix: Big space below the comment box on mobile  \[[#&#8203;61543](https://community.openproject.org/wp/61543)]
* Bugfix: Danger Dialog scroll behaviour still incorrect when used with forms \[[#&#8203;61546](https://community.openproject.org/wp/61546)]
* Bugfix: Meeting sets system user as invited \[[#&#8203;61570](https://community.openproject.org/wp/61570)]
* Bugfix: Time entry cannot be saved when comment is >255 characters long \[[#&#8203;61602](https://community.openproject.org/wp/61602)]
* Bugfix: XSS via time entry comment \[[#&#8203;61605](https://community.openproject.org/wp/61605)]
* Bugfix: Danger Dialogs should use "alertdialog" ARIA role \[[#&#8203;61630](https://community.openproject.org/wp/61630)]
* Bugfix: Meeting with an attachment cannot be deleted \[[#&#8203;61632](https://community.openproject.org/wp/61632)]
* Bugfix: My spent time: No error feedback when selecting work package without cost module enabled \[[#&#8203;61635](https://community.openproject.org/wp/61635)]
* Bugfix: Meeting series not destroyed together with project \[[#&#8203;61638](https://community.openproject.org/wp/61638)]
* Bugfix: Changing the work package on an existing time entry creates a new time entry instead of updating the old one \[[#&#8203;61657](https://community.openproject.org/wp/61657)]
* Bugfix: Cannot delete users who used emoji reactions \[[#&#8203;61708](https://community.openproject.org/wp/61708)]
* Bugfix: Cannot delete users who have work package reminders \[[#&#8203;61756](https://community.openproject.org/wp/61756)]

[3.33.2]
* Update openproject to 15.3.2
* [Full Changelog](https://www.openproject.org/docs/release-notes/15-3-0/)
* Bugfix: Meeting organiser does not receive their own meeting invites \[[#&#8203;61427](https://community.openproject.org/wp/61427)]
* Bugfix: Internal Server Error when export with open overview screen panel \[[#&#8203;61726](https://community.openproject.org/wp/61726)]
* Bugfix: PDF Export: table cell border/background styles definition in yml file is ignored \[[#&#8203;61749](https://community.openproject.org/wp/61749)]
* Bugfix: Form configuration requires reload - without reload the configuration of relations tables fail \[[#&#8203;61919](https://community.openproject.org/wp/61919)]
* Bugfix: Meeting series do not correctly handle daylight saving time switches \[[#&#8203;61933](https://community.openproject.org/wp/61933)]

[3.34.0]
* Base image 5.0.0

