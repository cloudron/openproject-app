FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y bison poppler-utils unrtf tesseract-ocr catdoc libxml2 && \
    rm -r /var/cache/apt /var/lib/apt/lists

# All dependency versions at https://github.com/opf/openproject/blob/dev/docker/prod/Dockerfile

# install pandoc with the required version > 2
RUN wget https://github.com/jgm/pandoc/releases/download/2.6/pandoc-2.6-1-amd64.deb && \
    dpkg -i pandoc-2.6-1-amd64.deb && \
    rm pandoc-2.6-1-amd64.deb

ARG NODE_VERSION=20.9.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

ENV RUBY_VERSION=3.4.1

# rbenv with ruby-build to install the required ruby version
RUN git clone https://github.com/rbenv/rbenv.git /usr/local/rbenv
ENV PATH /usr/local/rbenv/bin:$PATH
ENV RBENV_ROOT /home/cloudron/rbenv
RUN git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
RUN rbenv install ${RUBY_VERSION}
ENV PATH ${RBENV_ROOT}/versions/${RUBY_VERSION}/bin:$PATH

RUN gem install bundler --no-document

# renovate: datasource=github-releases depName=opf/openproject versioning=semver extractVersion=^v(?<version>.+)$
ARG OP_VERSION=15.3.2
RUN curl -L https://github.com/opf/openproject/archive/refs/tags/v${OP_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN bundle config set deployment 'true'
RUN bundle config set without 'mysql2 sqlite development test therubyracer docker'
# RUN bundle config set with 'activerecord-nulldb-adapter'
RUN bundle install

# we have to cd into frontend to install modules in /app/code/frontend/node_modules/ which is expected
RUN cd /app/code/frontend && npm install

COPY database.yml.build /app/code/config/database.yml

ENV RAILS_ENV="production"

RUN SECRET_KEY_BASE="building-assets" ./bin/rake assets:precompile

RUN rm -rf /app/code/config/database.yml        && ln -s /run/database.yml /app/code/config/database.yml && \
    rm -rf /app/code/config/secrets.yml         && ln -s /run/secrets.yml /app/code/config/secrets.yml && \
    rm -rf /app/code/tmp/                       && ln -s /run/openproject/tmp /app/code/tmp && \
    rm -rf /app/code/log/                       && ln -s /tmp/log /app/code/log && \
    rm -rf /app/code/db/schema.rb               && ln -s /app/data/schema.rb /app/code/db/schema.rb && \
    rm -rf /app/code/repositories/              && ln -s /app/data/repositories /app/code/repositories && \
    rm -rf /app/code/files                      && ln -s /app/data/files /app/code/files

COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/openproject/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh database.yml.template secrets.yml.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
