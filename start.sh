#!/bin/bash

set -eu

cd /app/code

IS_UPDATE="false"
if [[ -d /app/data/files ]]; then
    IS_UPDATE="true"
fi

# cannot use /tmp for tmp since rails gets confused about deleted cache files, in tmp!!
mkdir -p /run/openproject/tmp /tmp/log /app/data/repositories /app/data/files

export HOME=/run
export TMPDIR=/run/openproject/tmp

echo "=> Setup database configuration"
sed -e "s!##POSTGRESQL_HOST##!${CLOUDRON_POSTGRESQL_HOST}!" \
    -e "s!##POSTGRESQL_PORT##!${CLOUDRON_POSTGRESQL_PORT}!" \
    -e "s!##POSTGRESQL_USERNAME##!${CLOUDRON_POSTGRESQL_USERNAME}!" \
    -e "s!##POSTGRESQL_PASSWORD##!${CLOUDRON_POSTGRESQL_PASSWORD}!" \
    -e "s!##POSTGRESQL_DATABASE##!${CLOUDRON_POSTGRESQL_DATABASE}!" \
    /app/pkg/database.yml.template > /run/database.yml

# source custom overrides
if [[ ! -f /app/data/env.sh ]]; then
    echo -e '# Override env variables \n\n#export OPENPROJECT_LOG__LEVEL="info"\n' > /app/data/env.sh
fi
source /app/data/env.sh

# Setting mail from display name is not supported
export ADMIN_EMAIL="${CLOUDRON_MAIL_FROM}"
export OPENPROJECT_EMAIL__DELIVERY__METHOD="smtp"
export OPENPROJECT_SMTP__ADDRESS="${CLOUDRON_MAIL_SMTP_SERVER}"
export OPENPROJECT_SMTP__PORT="${CLOUDRON_MAIL_SMTP_PORT}"
export OPENPROJECT_SMTP__DOMAIN="${CLOUDRON_MAIL_DOMAIN}"
export OPENPROJECT_SMTP__AUTHENTICATION="plain"
export OPENPROJECT_SMTP__USER__NAME="${CLOUDRON_MAIL_SMTP_USERNAME}"
export OPENPROJECT_SMTP__PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"
export OPENPROJECT_SMTP__ENABLE__STARTTLS__AUTO="true"
export OPENPROJECT_HOST__NAME="${CLOUDRON_APP_DOMAIN}"

# for psql client
export PGPASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"

echo "=> Setting cookie secret"
SECRET_KEY_BASE=$(./bin/rails secret)
sed -e "s!##SECRET_KEY_BASE##!${SECRET_KEY_BASE}!" /app/pkg/secrets.yml.template > /run/secrets.yml

echo "=> Migrate database"
./bin/rake db:migrate

echo "=> Seed database if needed"
# We have to disable email delivery to avoid db:seed to send welcome emails to non-existing admin email account
export OPENPROJECT_EMAIL_DELIVERY_METHOD=""
./bin/rake db:seed || true
export OPENPROJECT_EMAIL_DELIVERY_METHOD="smtp"

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "=> Update LDAP config"
    psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c " \
        INSERT INTO ldap_auth_sources (id, name, host, port, account, account_password, base_dn, attr_login, attr_firstname, attr_lastname, attr_mail, onthefly_register, tls_mode, created_at, updated_at) \
        VALUES (1, 'Cloudron', '${CLOUDRON_LDAP_SERVER}', ${CLOUDRON_LDAP_PORT}, '${CLOUDRON_LDAP_BIND_DN}', '${CLOUDRON_LDAP_BIND_PASSWORD}', '${CLOUDRON_LDAP_USERS_BASE_DN}', 'username', 'givenName', 'sn', 'mail', TRUE, 0, NOW(), NOW()) \
        ON CONFLICT (id) DO UPDATE \
        SET name='Cloudron', host='${CLOUDRON_LDAP_SERVER}', port=${CLOUDRON_LDAP_PORT}, account='${CLOUDRON_LDAP_BIND_DN}', account_password='${CLOUDRON_LDAP_BIND_PASSWORD}', base_dn='${CLOUDRON_LDAP_USERS_BASE_DN}', attr_login='username', attr_firstname='givenName', attr_lastname='sn', attr_mail='mail', onthefly_register=TRUE, tls_mode=0, updated_at=NOW();"

    # only disable password reset for LDAP case
    psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value=0 WHERE name='self_registration';"
    psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value=0 WHERE name='lost_password';"
fi

echo "=> Update general config"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='https' WHERE name='protocol';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value=8 WHERE name='password_min_length';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='smtp' WHERE name='email_delivery_method';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_MAIL_SMTP_SERVER}' WHERE name='smtp_address';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_MAIL_SMTP_PORT}' WHERE name='smtp_port';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_MAIL_DOMAIN}' WHERE name='smtp_domain';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='plain' WHERE name='smtp_authentication';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_MAIL_SMTP_USERNAME}' WHERE name='smtp_user_name';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_MAIL_SMTP_PASSWORD}' WHERE name='smtp_password';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value=0 WHERE name='smtp_enable_starttls_auto';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_MAIL_FROM}' WHERE name='mail_from';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${CLOUDRON_APP_DOMAIN}' WHERE name='host_name';"
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value=1 WHERE name='email_login';"

echo "=> Clear previous cache to reflect db changes"
./bin/rake tmp:clear

echo "=> Fixup the directory permissions"
chown -R cloudron.cloudron /app/data /run /tmp

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OpenProject
