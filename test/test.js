#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = 300000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const PROJECT_NAME = 'cloudronproject';

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const adminUser = 'admin';
    const adminPassword = 'admin';
    const adminPasswordNew = 'Test1234567';

    var browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function loginFirstTime(username, password, newPassword = '') {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('(//input[@value="Sign in"])[2]')).click();

        if (newPassword) {
            await waitForElement(By.id('new_password'));
            await browser.findElement(By.id('password')).sendKeys(password);
            await browser.findElement(By.id('new_password')).sendKeys(newPassword);
            await browser.findElement(By.id('new_password_confirmation')).sendKeys(newPassword);
            await browser.findElement(By.xpath('(//button[@type="submit"])[1]')).click();
        }

        await browser.sleep(4000);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);

        await browser.sleep(2000);

        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('(//input[@value="Sign in"])[2]')).click();

        // wait for login to happen
        await browser.sleep(5000);

        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[@href="/notifications"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await waitForElement(By.xpath('//a[@title="Sign in"]'));

        await browser.manage().deleteAllCookies();
    }

    async function createProject() {
        await browser.get(`https://${app.fqdn}/projects/new`);
        await waitForElement(By.id('formly_3_textInput_name_0'));

        await browser.findElement(By.id('formly_3_textInput_name_0')).sendKeys(PROJECT_NAME);
        await browser.findElement(By.id('formly_3_textInput_name_0')).sendKeys(Key.RETURN);

        await browser.sleep(5000);
    }

    async function projectExists() {
        await browser.get(`https://${app.fqdn}/projects/${PROJECT_NAME}`);
        await waitForElement(By.xpath('//h2[contains(text(), "Project description")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', loginFirstTime.bind(null, adminUser, adminPassword, adminPasswordNew));
    it('can create project', createProject);
    it('project exists', projectExists);
    it('can logout', logout);

    it('can login', loginFirstTime.bind(null, username, password));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUser, adminPasswordNew));
    it('project exists', projectExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPasswordNew));
    it('project exists', projectExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
        await browser.sleep(5000);
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPasswordNew));
    it('project exists', projectExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app (update)', function () { execSync(`cloudron install --appstore-id org.openproject.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('get app information', getAppInfo);

    it('can admin login', loginFirstTime.bind(null, adminUser, adminPassword, adminPasswordNew));
    it('can create project', createProject);
    it('can logout', logout);

    it('can login', loginFirstTime.bind(null, username, password));
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can admin login', login.bind(null, adminUser, adminPasswordNew));
    it('project still exists after update', projectExists);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', async function () {
        browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
