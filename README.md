# OpenProject Cloudron App

This repository contains the Cloudron app package source for [OpenProject](https://openproject.org).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.openproject.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.openproject.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd openproject-app
cloudron build
cloudron install
```
